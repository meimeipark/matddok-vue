import {defineStore} from 'pinia';

interface IDeliveryRequest{
  id : number
  datePick : string // 주문일
  storeName : string // 업소명
  addressStoreDo : string // 가게주소
  addressClientDo : string // 고객주소
  askMenu : string // 주문메뉴
  requestStore : string // 가게 요청사항
  requestRider : string // 라이더 요청사항
  priceTotal : number // 결제 금액
  askId : number // 주문번호
  priceFood : number // 주문가격
  feeTotal : number // 배달비
  riderName : string // 라이더 이름
  feeAdmin : number // 관리자 수수료
  driveType : string // 배달수단
  phoneRider : string // 라이더 전화번호
  storeNumber : string // 가게 전화번호

}

// board 스토어
export const useDeliveryStore = defineStore('delivery', {
  state: () => ({
    deliverys: [],
    deliverym: {},
    totalCount: 0,
    totalPage: 0,
    currentPage:0,
  }),
  actions: {
    async fetchDelivery(pageNum:number) {
      const token = useCookie('token');
      const {data}:any = await useFetch(
        `http://matddak.shop:8080/v1/delivery/all/${pageNum}`, {
          method: 'GET',
          headers: {
            'Authorization': `Bearer ${token}`
          },
        },
      );
      if (data) {
        this.deliverys = data.value.list;
        this.currentPage = data.value.currentPage;
        // console.log(totalCountdata.value)
        this.totalPage = data.value.totalPage;
        this.totalCount = data.value.totalCount;
      }
    },
    async fetchDispatchDelivery(pageNum:number) {
      const token = useCookie('token');
      const {data}:any = await useFetch(
        `http://matddak.shop:8080/v1/delivery/all/pick/${pageNum}`, {
          method: 'GET',
          headers: {
            'Authorization': `Bearer ${token}`
          },
        },
      );
      if (data) {
        this.deliverys = data.value.list;
        this.currentPage = data.value.currentPage;
        // console.log(totalCountdata.value)
        this.totalPage = data.value.totalPage;
        this.totalCount = data.value.totalCount;
      }
    },
    async fetchDoneDelivery(pageNum:number) {
      const token = useCookie('token');
      const {data}:any = await useFetch(
        `http://matddak.shop:8080/v1/delivery/all/done/${pageNum}`, {
          method: 'GET',
          headers: {
            'Authorization': `Bearer ${token}`
          },
        },
      );
      if (data) {
        this.deliverys = data.value.list;
        this.currentPage = data.value.currentPage;
        // console.log(totalCountdata.value)
        this.totalPage = data.value.totalPage;
        this.totalCount = data.value.totalCount;
      }
    },
    async fetchStartDelivery(pageNum:number) {
      const token = useCookie('token');
      const {data}:any = await useFetch(
        `http://matddak.shop:8080/v1/delivery/all/go/${pageNum}`, {
          method: 'GET',
          headers: {
            'Authorization': `Bearer ${token}`
          },
        },
      );
      if (data) {
        this.deliverys = data.value.list;
        this.currentPage = data.value.currentPage;
        // console.log(totalCountdata.value)
        this.totalPage = data.value.totalPage;
        this.totalCount = data.value.totalCount;
      }
    },
    async fetchNotice(deliveryId:number) {
      const token = useCookie('token');
      const { id} = useRoute().params;
      const {data}:any = await useFetch(
        `http://matddak.shop:8080/v1/delivery/detail/${id}`, {
          method: 'GET',
          headers: {
            'Authorization': `Bearer ${token}`
          },
        },
      );

      if (data) {
        this.deliverym = data.value.data;
        console.log(data.value);
      }
    }
  }
})

if (import.meta.hot) {  //HMR
  import.meta.hot.accept(acceptHMRUpdate(useDeliveryStore, import.meta.hot))
}
