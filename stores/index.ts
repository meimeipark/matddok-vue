import {defineStore} from 'pinia';


// board 스토어
export const useIndexStore = defineStore('indexList', {
    state: () => ({
        dash: [],
        notice:[],
        income: {},
        charts: []
    }),
    actions: {
        async fetchIndex() {
            const token = useCookie('token');
            const {data}: any = await useFetch(
                `http://matddak.shop:8080/v1/dash/board`, {
                    method: 'GET',
                    headers: {
                        'Authorization': `Bearer ${token}`
                    },
                },
            );
            if (data.value) {
                this.notice = data.value.data.boardItems;
                console.log(data.value.data.boardItems)
            }
        },
        async fetchRiderIndex() {
            const token = useCookie('token');
            const {data}: any = await useFetch(
                `http://matddak.shop:8080/v1/dash/board`, {
                    method: 'GET',
                    headers: {
                        'Authorization': `Bearer ${token}`
                    },
                },
            );
            if (data.value) {
                this.dash = data.value.data.riderItems;
                console.log(data.value.data.riderItems)
            }
        },
        async fetchChartIndex() {
            const token = useCookie('token');
            const {data}: any = await useFetch(
                `http://matddak.shop:8080/v1/dash/board`, {
                    method: 'GET',
                    headers: {
                        'Authorization': `Bearer ${token}`
                    },
                },
            );
            if (data.value) {
                this.charts = data.value.data.nearDayStatistics;
                console.log(data.value.data.nearDayStatistics)
            }
        },

        async fetchIncome() {
            const token = useCookie('token');
            const {data}: any = await useFetch(
                `http://matddak.shop:8080/v1/income/statistics/admin`, {
                    method: 'GET',
                    headers: {
                        'Authorization': `Bearer ${token}`
                    },
                },
            );
            if (data.value) {
                this.income = data.value.data;
                console.log(data.value.data)
            }
        }
    }
})



if (import.meta.hot) {  //HMR
    import.meta.hot.accept(acceptHMRUpdate(useIndexStore, import.meta.hot))
}
