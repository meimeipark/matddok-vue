export interface UserPayloadInterface {
    email: string;
    password: string;
}

export interface INotice {
    id: number
    title: string
    text: string
    multipartFile: string
    img: string
}

export interface IRiderPage {
  totalCount: number
  totalPage: number
  currentPage: number
}

export interface INAdmin {
    name: string
    email: string
}
