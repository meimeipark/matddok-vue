# 맛딱드림 PROJECT
🗓 2024.03.05(화) ~ 2024-05-03(금)
*** 
![logo](./images/matddak_splash.gif)
> '맛딱드림'은 기존 배달 라이더 어플의 UI를 개선 하고 좀 더 쉽게 작동할 수 있도록 만들었습니다.
***

## 😄 개발자 소개 


* **GmJu** (PM): 백엔드 <a href = "https://gitlab.com/myungdon"><img alt="GitLab" src ="https://img.shields.io/badge/gitlab-FC6D26?style=flat&logo=gitlab&logoColor=white"/></a>
* **McKang** (PL): 앱 프론트엔드 <a href = "https://gitlab.com/kmjkali"><img alt="GitLab" src ="https://img.shields.io/badge/gitlab-FC6D26?style=flat&logo=gitlab&logoColor=white"/></a>
* **IuPark**: 웹 프론트엔드 <a href = "https://gitlab.com/meimeipark"><img alt="GitLab" src ="https://img.shields.io/badge/gitlab-FC6D26?style=flat&logo=gitlab&logoColor=white"/></a>
* **WbBae**: 앱 프론트엔드 <a href = "hhttps://gitlab.com/babasw135"><img alt="GitLab" src ="https://img.shields.io/badge/gitlab-FC6D26?style=flat&logo=gitlab&logoColor=white"/></a>
* **RgKim**: 백엔드 <a href = "https://gitlab.com/namho0172"><img alt="GitLab" src ="https://img.shields.io/badge/gitlab-FC6D26?style=flat&logo=gitlab&logoColor=white"/></a>
* **FxNull**: 앱 프론트엔드 <a href = "https://gitlab.com/hamsubin0628"><img alt="GitLab" src ="https://img.shields.io/badge/gitlab-FC6D26?style=flat&logo=gitlab&logoColor=white"/></a>
***

## 🛠️ Stacks
![vueJS](https://img.shields.io/badge/Vue.js-35495E?style=for-the-badge&logo=vue.js&logoColor=4FC08D)
![tailwind](https://img.shields.io/badge/Tailwind_CSS-38B2AC?style=for-the-badge&logo=tailwind-css&logoColor=white)
![nuxt3](https://img.shields.io/badge/nuxt3-00DC82?style=for-the-badge&logo=Nuxt.js&logoColor=white)
![ts](https://img.shields.io/badge/TypeScript-007ACC?style=for-the-badge&logo=typescript&logoColor=white)
***
## 🌐 프로젝트 주요 기능
![webthumnail](./images/matddak-web.png)
> **1. 관리자 web 페이지 로그인/로그아웃 처리 기능** 
  - DB에 등록된 회원 정보를 입력 후 일치 시에 로그인이 되며,
        일치 확인 후 토큰을 발급하여 로그인이 성공합니다.
   - 로그아웃 버튼 클릭 시 로그인된 계정 로그아웃 처리가 가능하게 처리하였습니다.
> 
> **2. 대시보드 서비스별 조회 기능**
   -  라이더 리스트, 공지사항, 수익에 대한 통계 내용 등을 간략하게 나타냅니다.
> 
> **3. 게시판 별 조회 기능**
   - 선택한 게시판 목록을 전체적으로 보여주는 리스트 조회가 가능합니다.
   - 리스트에서 클릭 시 상세 정보 확인이 가능합니다.
> 
> **4. 페이징 처리 기능**
   - 리스트를 한 화면에서 10개씩 페이지 처리하여 한페이지 확인이 가능합니다.
> 
> **5. 게시판 등록, 수정, 삭제 기능**
   - 관리자가 게시판 내용을 등록, 수정 및 삭제할 수 있습니다.


***
## ⛓ 프로젝트 아키텍처
![아키텍처](./images/Architecture.png)


